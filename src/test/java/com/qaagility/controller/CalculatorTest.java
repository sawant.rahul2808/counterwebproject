package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {

  @Test
  public void testAdd() {
    assertEquals("testing add",9,new Calculator().add());
  }
  
  @Test
  public void testAddNegative() {
    assertNotEquals("testing add",7,new Calculator().add());
  }
}
